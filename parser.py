from music21 import *
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import sys

def extract_notes(midiPartiture):
    ret = []
    for nt in midiPartiture.flat.notes:        
        if isinstance(nt, note.Note):
            ret.append(nt.pitch.ps)
        elif isinstance(nt, chord.Chord):
            tmp = []
            for pitch in nt.pitches:
                tmp.append(pitch.ps)
            ret.append(tmp)
    return ret

def show_partiture(midiPartiture):
    print(len(midiPartiture.parts))
    for i in range(len(midiPartiture.parts)):
        top = midiPartiture.parts[i].flat.notes                  
        y = extract_notes(top)
        print(y)
        
def main():
  midiFile = sys.argv[1]
  
  midiPartiture = converter.parse(midiFile)
  partitureStream = midiPartiture.parts.stream()
  
  print("Debug only - Instruments")
  for e in partitureStream:
    print(e.partName)
    
  show_partiture(midiPartiture)

if __name__ == '__main__':
    main()
